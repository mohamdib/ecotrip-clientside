export let RESTAURANTS = [
{
    id: 1,
    name: "חומוס אבו שוקרי",
    address: "אבו גוש , רחוב השלום 16",
    phone: "84903246963",
    location: {
      lat: 31.8063,
      lon: 35.1092,
      distance: 3.2
    },
    rating: 4.5,
    scores: [
      {
        id: 1,
        name: "אוכל",
        score: 98
      },
      {
        id: 2,
        name: "משפחה",
        score: 80
      },
      {
        id: 3,
        name: "מקומי",
        score: 71
      }
    ],
    thumb: "http://www.coupo.co.il/sites/coupo2/_media/deals/0/110_dl_photo_slider1_4312c.jpg",
    reviews: [
      {
        id: 1,
        username: "יואל גבע",
        avatar: "https://d2cugjmsg1fopp.cloudfront.net/wp-content/uploads//2017/01/Muhammad-Ibrahim-sitting.jpg",
        from: "חיפה",
        content: "אחלה מסעדה , אכלתי בה הרבה  ואני הולך שוב לבקר שם , ממלחיץ ובחום חברים ! ",
        rating: 4,
        recommended_for: [1, 3]
      }
    ]
  },
  {
    id: 2,
    name: "מסעדת נאג'י",
    address: "מחמוד רשיד 4 , אבו גוש",
    phone: "02-5345167",
    location: {
      lat: 31.807917,
      lon: 35.107603,
      distance: 3.2
    },
    rating: 4.2,
    scores: [
      {
        id: 1,
        name: "מחיר",
        score: 98
      },
      {
        id: 2,
        name: "משפחה",
        score: 80
      },
      {
        id: 3,
        name: "מקומי",
        score: 71
      },
      {
        id: 4,
        name: "אוכל",
        score: 64
      }
    ],
    thumb: "http://lh4.ggpht.com/_mcpK53rsBBc/TTyiiuIJeiI/AAAAAAAAEU0/oMLiXgfqT-I/s512/P1230003.JPG",
    reviews: []
  }/*,
  {
    id: 3,
    name: "Green Tangerine",
    address: "48 Hang Be, Hanoi, Vietnam",
    phone: "84 4 3825 1286",
    location: {
      lat: 21.030736,
      lon: 105.812923,
      distance: 3.2
    },
    rating: 4.5,
    scores: [
      {
        id: 1,
        name: "Foodies",
        score: 98
      },
      {
        id: 2,
        name: "Family",
        score: 80
      },
      {
        id: 3,
        name: "Local",
        score: 71
      },
      {
        id: 4,
        name: "Budget",
        score: 64
      },
      {
        id: 5,
        name: "Adventure",
        score: 59
      }
    ],
    thumb: "assets/img/restaurant/thumb/img_3.jpg",
    reviews: []
  },
  {
    id: 4,
    name: "KOTO Restaurant",
    address: "61 Pho Van Mieu, Hanoi Vietnam",
    phone: "+84 4 747 0337",
    location: {
      lat: 21.030756,
      lon: 105.811933,
      distance: 3.2
    },
    rating: 4.2,
    scores: [
      {
        id: 1,
        name: "Adventure",
        score: 98
      },
      {
        id: 2,
        name: "Local",
        score: 80
      },
      {
        id: 3,
        name: "Foodies",
        score: 71
      },
      {
        id: 4,
        name: "Budget",
        score: 64
      },
      {
        id: 5,
        name: "Family",
        score: 59
      }
    ],
    thumb: "assets/img/restaurant/thumb/img_4.jpg",
    reviews: []
  },
  {
    id: 5,
    name: "Hanoi House",
    address: "Level 2, 47A Ly Quoc Su St., Hanoi 10000, Vietnam",
    phone: "+844.23489789",
    location: {
      lat: 21.031746,
      lon: 105.812913,
      distance: 3.2
    },
    rating: 4.5,
    scores: [
      {
        id: 1,
        name: "Foodies",
        score: 98
      },
      {
        id: 2,
        name: "Family",
        score: 80
      },
      {
        id: 3,
        name: "Local",
        score: 71
      },
      {
        id: 4,
        name: "Budget",
        score: 64
      },
      {
        id: 5,
        name: "Adventure",
        score: 59
      }
    ],
    thumb: "assets/img/restaurant/thumb/img_5.jpg",
    reviews: []
  },
  {
    id: 6,
    name: "Nhạc Cafe",
    address: "7 Hang Thung | Quan Hoan Kiem, Hanoi 10000, Vietnam",
    phone: "84439352580",
    location: {
      lat: 21.030746,
      lon: 105.811913,
      distance: 3.2
    },
    rating: 4.5,
    scores: [
      {
        id: 1,
        name: "Foodies",
        score: 98
      },
      {
        id: 2,
        name: "Family",
        score: 80
      },
      {
        id: 3,
        name: "Local",
        score: 71
      },
      {
        id: 4,
        name: "Budget",
        score: 64
      },
      {
        id: 5,
        name: "Adventure",
        score: 59
      }
    ],
    thumb: "assets/img/restaurant/thumb/img_6.jpg",
    reviews: []
  }*/
]
