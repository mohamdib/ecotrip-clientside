import {Component} from "@angular/core";
import {NavController,AlertController,ModalController} from "ionic-angular";
import {ActivityService} from "../../services/activity-service";
import { Http ,RequestOptions  } from '@angular/http';
import localForage from "localforage";
import { NativeStorage } from '@ionic-native/native-storage';
import {AttractionSelectPage} from '../attraction-select/attraction-select';
import {MapPagePage} from '../pages/map/map';


@Component({
  selector: 'page-activity',
  templateUrl: 'activity.html'
})
export class ActivityPage {
  // activities
  public activities: any;
    public attractions: any;
    public favorite: any;
    public value : any;
    public datosh: any;
  constructor(public nav: NavController,public storage: NativeStorage, private alertCtrl: AlertController,
    public modalCtrl: ModalController, public activityService: ActivityService,private http: Http) {
   



   /* this.activities = activityService.getAll();
    this.favorite=this.storage.getItem('favorite').then((data2)=>{
      console.log(data2);
      let data3={ids:data2};
        this.http.post("http://ecotr.herokuapp.com/api/attraction/favorite",data3,null).map(res => res.json()).subscribe((data)=>{
           console.log(data);
           this.attractions=data;
        } );

});*/

 

 



   // console.log(arr);

   // this.favorite.map((id)=>{console.log(id)});
  /*    this.http.post("http://localhost:8000/api/attraction/favorite",this.favorite).map(res => res.json()).subscribe((data)=>
       {
         this.attractions=data
       // console.log(data);
       });*/





       // shake //

       
  }
  presentAlert(msg) {
  let alert = this.alertCtrl.create({
    title: 'alert',
    subTitle: msg,
    buttons: ['Dismiss']
  });
  alert.present();
}
toInt(rating)
  {
   return  parseInt(rating);

  }
  remove(id)
  {
    this.storage.getItem('favs').then((data2)=>{
     let dat=<Array<String>>data2.ids;
     let index=dat.indexOf(id);
     if (index > -1) {
       dat.splice(index, 1);
     this.storage.setItem('favs',{ids:dat});
     let data3={ids:dat};
     this.http.post("http://ecotr.herokuapp.com/api/attraction/favorite",data3,null).map(res => res.json()).subscribe((data)=>{
           this.attractions=data;
        } );
       }
    });
   

  }
  ionViewDidLoad() {
    this.storage.getItem('favs').then((data)=>{
       if(!data || data==null){

       }
       else{
          this.storage.getItem('favs').then((data2)=>{
        this.http.post("http://ecotr.herokuapp.com/api/attraction/favorite",data2,null).map(res => res.json()).subscribe((data)=>{
           this.attractions=data;
        } );

});
       }
 },
 (error)=>{

 });
  /*  this.favorite=this.storage.getItem('favorite').then((data2)=>{
      console.log(data2);
      let data3={ids:data2};
        this.http.post("http://ecotr.herokuapp.com/api/attraction/favorite",data3,null).map(res => res.json()).subscribe((data)=>{
           console.log(data);
           this.attractions=data;
        } );
    });
    this.datosh=JSON.stringify(this.favorite);*/
     

  /*   this.storage.getItem('favs').then((data2)=>{
        this.http.post("http://ecotr.herokuapp.com/api/attraction/favorite",data2,null).map(res => res.json()).subscribe((data)=>{
           this.attractions=data;
        } );

      });*/
}
  openCardAlert(id){
     let modal = this.modalCtrl.create(AttractionSelectPage,{"id":id});
     modal.present();
  }
  // make array with range is n
  range(n) {
    return new Array(Math.round(n));
  }

  // toggle like an activity
  toggleLike(activity) {
    activity.is_liked = !activity.is_liked;
  }

deleteAll(){
      let alert = this.alertCtrl.create({
        title: 'מחיקת מועדפים',
        message: 'האם ברצונך למחוק את כל המועדפים שלך ?',
        buttons: [
          {
            text: 'בטל',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'מחק',
            handler: () => {
              this.attractions=null;
              let lfavs={
                ids:[String]
              };
              this.storage.setItem("favs",{ids:[String]});
              console.log('delete clicked');
            }
          }
        ]
      });
      alert.present();
    }


}
