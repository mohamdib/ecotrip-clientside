import {
  Component
} from '@angular/core';
import {
  NavController,
  NavParams
} from 'ionic-angular';
import {
  Alert,
  AlertController,
  ModalController
} from 'ionic-angular';
import {
  AttractionSelectPage
} from '../attraction-select/attraction-select';
import localForage from "localforage";
import {
  Ionic2RatingModule
} from 'ionic2-rating';
import {
  NativeStorage
} from '@ionic-native/native-storage';
import {
  ActivityPage
} from '../activity/activity';
import {
  Toast
} from '@ionic-native/toast';

/*
  Generated class for the SearchResults page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-search-results',
  templateUrl: 'search-results.html'
})
export class SearchResultsPage {
  results = [];
  mychoice = "";
  types = [];
  res = {
    type: ""
  };
  people: any;
  local: any;
  datosh: any;
  ids: any = [];

  constructor(public navCtrl: NavController, private toast: Toast, public navParams: NavParams, public alertCtrl: AlertController, public modalCtrl: ModalController, private storage: NativeStorage) {
    // this.local = new Storage(LocalStorage);
    // this.local.set('didTutorial', 'true');
    this.storage.setItem('firstTime', {
        isFirst: '1'
      })
      .then(
        () => console.log('Stored item!'),
        error => console.error('Error storing item', error)
      );
    this.storage.getItem('favs').then(
      (data) => {
        if (data == null || !data)
          this.storage.setItem('favs', {
            ids: [String]
          });
      }, (error) => console.log(error));
    this.storage.getItem('firstTime')
      .then(
        data => {
          console.log(data);
          if (data.isFirst == '1') {
            this.datosh = "yes is 1";
            this.storage.setItem('firstTime', {
              isFirst: '2'
            });
          } else {
            this.datosh = "No im not 1";
          }
        },
        error => console.error(error)
      );

  }
  toInt(rating) {
    return parseInt(rating);

  }
  isInFavorits(id) {
    if (this.local !== undefined) {
      this.local.map((id2) => {
        if (id2 == id) {
          return true;
        }
      });
      return false;
    }
    return false;

  }
  presentAlert(msg) {
    let alert = this.alertCtrl.create({
      title: 'תודה',
      subTitle: msg,
      buttons: ['אישור']
    });
    alert.present();
  }
  makeToast() {
    //addToFavorite(res._id);
    this.toast.show('Im a toast', '5000', 'center');
  }
  toFavoritePage() {
    this.navCtrl.push(ActivityPage);
  }
  ionViewDidLoad() {
    /* this.storage.getItem("favs").then((result) => {
                 this.people = result ? <Array<String>> result : [];
                 this.local=result;   
                 console.log(this.local);             
             }, (error) => {
                 console.log("ERROR: ", error);
             });*/
    this.results = this.navParams.get('data');
    this.mychoice = this.navParams.get('mychoice');
  }

  openFilters() {

  }
  openCardAlert(id) {
    let modal = this.modalCtrl.create(AttractionSelectPage, {
      "id": id
    });
    modal.present();
  }



  // add id to favorite  - before , check if it already exist ! 
  addToFavorite(id) {

    let lfavs = {
      ids: [String]
    };
    let bol = false;
    // check if id already  exist ! 
    this.storage.getItem('favs')
      .then(
        data => {
          lfavs = data;

          lfavs.ids.map((mid) => {
            if (mid == id)
              bol = true;

          });
          if (bol == false) {
            lfavs.ids.push(id);
            this.datosh = lfavs;
            this.storage.setItem('favs', lfavs);
            this.presentAlert("נוסף למועדפים !");
          }

        },
        error => {
          this.datosh = "im in error state ! ";

        }
      );

  }

}
