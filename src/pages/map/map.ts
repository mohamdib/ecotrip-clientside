import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {
 GoogleMaps,
 GoogleMap,
 GoogleMapsEvent,
 LatLng,
 CameraPosition,
 MarkerOptions,
 Marker
} from '@ionic-native/google-maps';
declare var google;
@Component({
  selector: 'page-map',
  templateUrl: 'map.html'
})
export class MapPagePage {
 directionsService = new google.maps.DirectionsService;

  constructor(public navCtrl: NavController, public navParams: NavParams) {



  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MapPagePage');
    this.initMap();
  }



initMap() {
       var markerArray = [];

        let map2 = new google.maps.Map(document.getElementById('map'), {
          zoom: 7,
          center: {lat: 31.7683, lng: 35.2137}
        });
         let directionsDisplay = new google.maps.DirectionsRenderer({'map':map2});
         var stepDisplay = new google.maps.InfoWindow;
        this.calculateAndDisplayRoute(this.directionsService,directionsDisplay,markerArray,stepDisplay,map2);
      }

     calculateAndDisplayRoute(directionsService,directionsDisplay,markerArray,stepDisplay,map) {

        for (var i = 0; i < markerArray.length; i++) {
          markerArray[i].setMap(null);
        }


        this.directionsService.route({
          origin: 'ירושלים',
          destination: 'תל אביב',
          travelMode: 'WALKING'
        }, function(response, status) {
          if (status === 'OK') {
          	console.log('response:',response);
             document.getElementById('warnings-panel').innerHTML =
                '<b>' + response.routes[0].warnings + '</b>';
            directionsDisplay.setDirections(response);
            this.showSteps(response, markerArray, stepDisplay, map);
            console.log('success');
          } else {
          	 console.log('failed:',status);
            window.alert('Directions request failed due to ' + status);
          }
        });
        
      }

      showSteps(directionResult, markerArray, stepDisplay, map) {
        // For each step, place a marker, and add the text to the marker's infowindow.
        // Also attach the marker to an array so we can keep track of it and remove it
        // when calculating new routes.
        var myRoute = directionResult.routes[0].legs[0];
        for (var i = 0; i < myRoute.steps.length; i++) {
          var marker = markerArray[i] = markerArray[i] || new google.maps.Marker;
          marker.setMap(map);
          marker.setPosition(myRoute.steps[i].start_location);
          this.attachInstructionText(
              stepDisplay, marker, myRoute.steps[i].instructions, map);
        }
      }
      attachInstructionText(stepDisplay, marker, text, map) {
        google.maps.event.addListener(marker, 'click', function() {
          // Open an info window when the marker is clicked on, containing the text
          // of the step.
          stepDisplay.setContent(text);
          stepDisplay.open(map, marker);
        });
      }
}
