import { Component } from '@angular/core';
import { NavController, NavParams ,ModalController} from 'ionic-angular';
import {AttractionSelectPage} from '../attraction-select/attraction-select';
import {
 GoogleMaps,
 GoogleMap,
 GoogleMapsEvent,
 LatLng,
 CameraPosition,
 MarkerOptions,
 Marker
} from '@ionic-native/google-maps';

declare var google;

@Component({
  selector: 'page-best-path-results',
  templateUrl: 'best-path-results.html'
})
export class BestPathResultsPagePage {
results=[];
 		 directionsService = new google.maps.DirectionsService;
    mychoice="";
    types=[];
    res={
        type: ""
    };
    people: any;
    local:any;
    showBy;
    datosh: any;
    ids : any = [];
    showmap;
    showlist;
    firstTime;
  constructor(public navCtrl: NavController,private googleMaps: GoogleMaps, public navParams: NavParams,public modalCtrl: ModalController) {
  			this.showlist=false;
  			this.showmap=true;
        this.showBy='m';
        this.firstTime=0;
  }

 ionViewDidLoad() {
        this.results=this.navParams.get('data');
        this.results.map((res,index)=>{
        	this.results.map((res2,index2)=>{
        		if((res._id==res2._id) && (index!=index2))
        			this.results.splice(index, 1);;
        	})
        });
        this.mychoice=this.navParams.get('mychoice');
        this.initMap();



  }
  openCardAlert(id){
     let modal = this.modalCtrl.create(AttractionSelectPage,{"id":id});
     modal.present();

  }
   toInt(rating)
  {
   return  parseInt(rating);

  }
   initMap() {
       
        let map2 = new google.maps.Map(document.getElementById('map'), {
          zoom: 7,
          center: {lat: 31.7683, lng: 35.2137}
        });
         let directionsDisplay = new google.maps.DirectionsRenderer({'map':map2});
        this.calculateAndDisplayRoute(this.directionsService,directionsDisplay);
      }

     calculateAndDisplayRoute(directionsService,directionsDisplay) {
         var waypts = [];

         this.results.map((res,index)=>{
           if(index!=0 && index!=this.results.length-1){
             console.log(index+":"+this.results[index].city);
              waypts.push({
                  location: res.city,
                  stopover: true
                });
           }
         });
         

        this.directionsService.route({
          origin: this.results[0].city ,
          destination: this.results[this.results.length-1].city,
          waypoints: waypts,
          optimizeWaypoints: true,
          travelMode: 'DRIVING'
        }, function(response, status) {
          if (status === 'OK') {
          	console.log('response:',response);
            directionsDisplay.setDirections(response);
            console.log('success');
          } else {
          	 console.log('failed:',status);
            window.alert('Directions request failed due to ' + status);
          }
        });
        
      }

    showMap()
    {
    	this.showmap=true;
    	//this.initMap();
    }
     showList()
    {
    	this.showmap=false;
    }
}
